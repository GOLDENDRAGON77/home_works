let number1 = +prompt("Please, enter first number");
number1 = validateNumber(number1);
let number2 = +prompt("Please, enter second number");
number2 = validateNumber(number2);

let operation = prompt("Please, choose mathematical operation(+, -, *, /)");

function mathOperation() {
    switch (operation) {
        case "+":
            return number1 + number2;
        case "-":
            return number1 - number2;
        case "*":
            return number1 * number2;
        case "/":
            return number1 / number2;
        default:
            return "Incorrect operation";
    }
}

function validateNumber(number) {
    while (isNaN(number) || !number) {
        number = +prompt("Please, enter number once more");
    }
    return number;
}

console.log(mathOperation());
