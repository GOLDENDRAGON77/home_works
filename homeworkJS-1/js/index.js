"use strict";

function validNumber(n) {
    return /^[0-9]+$/.test(n);
}

function validName(n) {
    return /^[a-zA-Z]+$/.test(n);
}

let name = prompt("Enter you name");
while (!validName(name)) {
    name = prompt("Enter you name", name);
}

let age = prompt("How old are you");
while (!validNumber(age) && age != null) {
    age = prompt("How old are you");
}

if (age < 18) {
    alert("You are not allowed to visit this website.")
} else if (age >= 18 && age <= 22) {
    let result = confirm("Are you sure you want to continue?");
    if (result) {
        alert("Welcome " + name)
    } else {
        alert("You are not allowed to visit this website.")
    }
} else {
    alert("Welcome " + name)
}
