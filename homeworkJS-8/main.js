function inputFocus(event) {
    priceInput.className = "price";
    this.classList.add("input-border");
    const errors = document.getElementById("error-label");
    if (!errors.classList.contains("d-none")) {
        errors.classList.add("d-none");
    }

}

function inputBlur(event) {
    const inputValue = priceInput.value;
    if (!inputValue || isNaN(inputValue) || +inputValue <= 0 ) {
        priceInput.classList.add("errorInput");
        document.getElementById("error-label").classList.remove("d-none");
        return;
    }
    this.classList.remove("input-border");
    currPrice.classList.remove("d-none");
    const div = document.createElement("div");
    const span = document.createElement("span");
    span.innerHTML = `Current price: ${inputValue} $ `;
    const btn = document.createElement("button");
    btn.innerText = "X";
    btn.addEventListener("click", function (event) {
        div.remove();
        priceInput.value = "";
    });
    div.classList.add("currPrice");
    div.appendChild(span);
    div.appendChild(btn);
    btn.classList.add("btnStyle");
    priceInput.classList.add("valueInput");
    currPrice.appendChild(div);
    console.log();

}

priceInput.addEventListener('focus', inputFocus);
priceInput.addEventListener('blur', inputBlur);

//function sum span price.
function calculate() {
    let sum = 0;
    [...document.querySelectorAll(".currPrice span")].forEach(item => {
        sum += +item.innerText.split(" ")[2]
    });
    return sum;
}
