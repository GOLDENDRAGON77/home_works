let currentIndex = 0;
let flag = true;
const images = [...document.getElementsByClassName("image-to-show")];

function enter() {
    fadeOut(images[currentIndex]);
    // .classList.add("img-none");
    currentIndex++;
    if (currentIndex >= images.length) {
        currentIndex = 0;
    }
    fadeIn(images[currentIndex]);
    // .classList.remove("img-none");
}

function timerdown() {
    milliseconds.innerHTML--;
    if (milliseconds.innerHTML <= 0) {
        seconds.innerHTML--;
        milliseconds.innerHTML = "100";
    }
    if (seconds.innerHTML <= 0) {
        seconds.innerHTML = "9";
        enter()
    }
}

let seconds = document.getElementById("time");
let timerdownInterval = setInterval(timerdown, 10);

document.getElementById("pause").addEventListener("click", function () {
    clearInterval(timerdownInterval);
    flag = false;
});

document.getElementById("play").addEventListener("click", function () {
    if (!flag) {
        flag = true;
        timerdownInterval = setInterval(timerdown, 10);
    }
});

function fadeIn(element) {
    element.style.opacity = parseFloat(element.style.opacity || 0) + 0.2;
    if (!(parseFloat(element.style.opacity) > 1.0)) {
        setTimeout(() => fadeIn(element), 100);
    }
}

function fadeOut(element) {
    element.style.opacity = parseFloat(element.style.opacity || 1) - 0.2;
    if (!(parseFloat(element.style.opacity) < 0.0)) {
        setTimeout(() => fadeOut(element), 100);
    }
}

