const menu = document.getElementById("theMenu");
const menuItems = [...document.getElementsByClassName("tabs-title")];
const liNone = [...document.getElementsByClassName("li-none")];

let activeIndex = 0;

menu.addEventListener("click", function (e) {
    menuItems[activeIndex].classList.remove("active");
    liNone[activeIndex].classList.remove("active");

    activeIndex = menuItems.indexOf(e.target);

    menuItems[activeIndex].classList.add("active");
    liNone[activeIndex].classList.add("active");
});


