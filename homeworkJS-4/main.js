function createNewUser() {
    const newUser = {};

    Object.defineProperty(newUser, 'firstName', {
        value: prompt("Enter your First Name:"),
        writable: false,
        configurable: true
    });
    Object.defineProperty(newUser, 'lastName', {
        value: prompt("Enter your Last Name:"),
        writable: false,
        configurable: true
    });

    newUser.setFirstName = function(value) {
        Object.defineProperty(newUser, 'firstName', {
            value: value
        });
    };

    newUser.setLastName = function(value) {
        Object.defineProperty(newUser, 'lastName', {
            value: value
        });
    };

    function getLogin() {
        return newUser.firstName.charAt(0).toLowerCase() + newUser.lastName.toLowerCase();
    }
    newUser.getLogin = getLogin;

    return newUser;
}

const user = createNewUser();
console.log(user.getLogin());
user.setFirstName("new Name");
console.log(user.firstName);
user.setLastName("new Second");
console.log(user.lastName);
